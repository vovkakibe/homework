package third;

public class Main {

    public static void main(String[] args) {
        Student firstStudent = new Student("vasya", "petrov", "little", 5D);
        Student twoStudent = new Student("ilya", "ilyasov", "middle", 2D);

        Student three = new Aspirant("petr", "maksimov", "young", 4D);

        Student[] array = {firstStudent, twoStudent, three};

        for (int i = 0; i < array.length; i++) {
            String name = array[i].getFirstName();
            String lastName = array[i].getLastName();
            String group = array[i].getGroup();
            Double average = array[i].getAverageMark();
            int summ = array[i].getScholarship(array[i].getAverageMark());

            System.out.println("name " + name + " lastName " + lastName + " group " + group + " average " + average + "  summ " + summ);
        }
    }
}
