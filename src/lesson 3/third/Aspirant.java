package third;

public class Aspirant extends Student {

    public Aspirant(String firstName, String lastName, String group, Double averageMark) {
        super(firstName, lastName, group, averageMark);
    }

    @Override
    public int getScholarship(Double averageMark) {
        int summ = 0;
        if (averageMark == 5) {
            summ = 200;
        } else {
            summ = 180;
        }
        return summ;
    }
}
