package second;

public abstract class Animal {
    private String food;
    private String location;

    void makeNoise() {
        System.out.println("Не определено");
    }

    void eat() {
        System.out.println("Не определено");
    }

    void sleep() {

    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
