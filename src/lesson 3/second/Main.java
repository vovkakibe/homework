package second;

public class Main {
    private static final Veterinar veterinar = new Veterinar();


    public static void main(String[] args) {
        Animal cat = new Cat();

        cat.setFood("meet");
        cat.setLocation("home");

        Animal dog = new Dog();

        dog.setFood("meet");
        dog.setLocation("Street");

        Animal horse = new Horse();

        horse.setFood("grass");
        horse.setLocation("pasture");

        Animal[] animals = {cat, dog, horse};

        for (Animal animal : animals) {
            veterinar.treatAnimal(animal);
        }
    }
}
