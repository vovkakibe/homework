package first;

public class RealizationPhone {
    public static void main(String[] args) {
        Phone moto = new Phone(230232, "moto", 12);
        Phone sony = new Phone(230234, "sony", 34);
        Phone htc = new Phone(230235, "htc", 56);

        System.out.println(moto.toString());
        System.out.println(sony.toString());
        System.out.println(htc.toString());

        moto.receiveCall("Вася");
        sony.receiveCall("Петя");
        htc.receiveCall("Надя");

        int numberMoto = moto.getNumber();
        int numberSony = sony.getNumber();
        int numberHtc = htc.getNumber();

        moto.receiveCall("Вася", moto.getNumber());
        sony.receiveCall("Петя", sony.getNumber());
        htc.receiveCall("Надя", htc.getNumber());


        Integer[] numbers = {moto.getNumber(), sony.getNumber(), htc.getNumber()};

        moto.sendMessage(numbers);

    }

}
