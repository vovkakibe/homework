package second;

public class SportCar extends Car {
    int maxSpeed;


    public SportCar() {
        Engine engine = new Engine(14,11);
        setBrand("audi");
        setClassAuto("sport");
        setType(engine);
        setWeight(1);
    }

    @Override
    void start() {
        maxSpeed = 250;
        System.out.println("Sport car поехал");
    }

    @Override
    void stop() {
        maxSpeed = 0;
        System.out.println("Sport car остановился");
    }

    @Override
    void printInfo() {


    }


}
