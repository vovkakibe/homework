package second;

public abstract class Car {
    private String brand;
    private String classAuto;
    private int weight;
    private Engine type;

    abstract void start();

    abstract void stop();

    void turnRight() {
        System.out.println("Поворот направо");
    }

    void turnLeft() {
        System.out.println("Поворот налево");
    }

    abstract void printInfo();


    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getClassAuto() {
        return classAuto;
    }

    public void setClassAuto(String classAuto) {
        this.classAuto = classAuto;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Engine getType() {
        return type;
    }

    public void setType(Engine type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Cars{" +
                "brand='" + brand + '\'' +
                ", classAuto='" + classAuto + '\'' +
                ", weight='" + weight + '\'' +
                ", type=" + type +
                '}';
    }
}
