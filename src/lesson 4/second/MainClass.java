package second;

public class MainClass {
    public static void main(String[] args) {
        Lorry lorry = new Lorry(12);

        Engine engine = new Engine(12,51);

        lorry.setBrand("mercedes");
        lorry.setClassAuto("Lorry");
        lorry.setType(engine);
        lorry.setWeight(12);

        System.out.println(lorry);


        lorry.start();
        lorry.stop();

        SportCar car = new SportCar();

        System.out.println(car);

        car.start();
        car.stop();
    }
}
