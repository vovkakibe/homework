package second;

public class Lorry extends Car {
    private int carrying;

    public Lorry(int carrying) {
        this.carrying = carrying;
    }


    @Override
    void start() {
        System.out.println("Грузовик поехал");
    }

    @Override
    void stop() {
        System.out.println("грузовик остановился");
    }

    @Override
    void printInfo() {


    }


    public int getCarrying() {
        return carrying;
    }

    public void setCarrying(int carrying) {
        this.carrying = carrying;
    }
}
