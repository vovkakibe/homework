package first;

public class Triangle implements Shape {
    double sideOne;
    double sideTwo;
    double sideThree;

    public Triangle(double sideOne, double sideTwo, double sideThree) {
        this.sideOne = sideOne;
        this.sideTwo = sideTwo;
        this.sideThree = sideThree;
    }

    @Override
    public Double square() {
        double s = (sideOne + sideTwo + sideThree) / 2.0;

        return s;
    }
}
