package first;

public class Circle implements Shape {
    double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public Double square() {
        double area = radius *  radius *  Math.PI;

        return area;
    }
}
