package first;

public class Main {
    public static void main(String[] args) {
        Shape[] shape = new Shape[3];
        shape[0] = new Triangle(1.2,1.2,1.2);
        shape[1] = new Circle(2.3);
        shape[2] = new Rectangle(1.2,2.3);
        for (Shape sh: shape){
            System.out.println("Площадь фигуры: "+sh.square());
        }
    }

}
