package first;

public class Rectangle implements Shape {
    double sideOne;
    double sideTwo;

    public Rectangle(double sideOne, double sideTwo) {
        this.sideOne = sideOne;
        this.sideTwo = sideTwo;
    }

    @Override
    public Double square() {

        Double p = sideOne * sideTwo;

        return p;
    }
}
