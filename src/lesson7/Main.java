package lesson7;

import java.io.IOException;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) throws IOException {

        ServiceReader.checkAndCreateFile("C:\\project\\1.txt");

        Scanner in = new Scanner(System.in);
        System.out.print("Input a command: ");

        AccountImpl account = new AccountImpl();

        if (in.hasNext()) {
            String command = in.nextLine();
            String[] commandData = command.split("\\s+");
            int id = Integer.parseInt(commandData[1].replaceAll("\\[", "").replaceAll("]", ""));

            if (!command.contains("transfer")) {

                if (command.contains("balance")) {

                    account.balance(id);
                } else {
                    int amount = Integer.parseInt(commandData[2].replaceAll("\\[", "").replaceAll("]", ""));

                    if (command.contains("withdraw")) {

                        account.withdraw(id, amount);

                    } else if (command.contains("deposit")) {

                        account.deposit(id, amount);

                    }

                }
            } else {
                int to = Integer.parseInt(commandData[2].replaceAll("\\[", "").replaceAll("]", ""));
                int amount = Integer.parseInt(commandData[3].replaceAll("\\[", "").replaceAll("]", ""));

                account.transfer(id, to, amount);
            }
        }

    }
}
