package lesson7;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class ServiceReader {

    private static String charset(String value){
        Charset cset = StandardCharsets.UTF_8;

        ByteBuffer buffer = cset.encode (value);
        byte[] bytes = buffer.array();
        ByteBuffer bbuf = ByteBuffer.wrap (bytes);
        CharBuffer cbuf = cset.decode(bbuf) ;
        return cbuf.toString();
    }

    public static ArrayList<String> readData(String fileName) {
        ArrayList<String> answer = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while(reader.ready() && (line = reader.readLine()) != null){
               answer.add(line);

            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return answer;
    }

    public static void write(String filename, ArrayList<String> data) {

        try  {
            PrintWriter writer = new PrintWriter(new FileWriter(filename),true);
            for (String dataOut : data) {
                writer.println(dataOut);
            }
        writer.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void checkAndCreateFile(String filename) throws IOException {
        File file = new File(filename);

        if(!file.exists()){
            ArrayList<String> testData = new ArrayList<>();

            testData.add("12;Vladimir;1200");
            testData.add("14;petr;1233");
            testData.add("15;Aleks;1444");
            testData.add("16;John;6432");
            testData.add("17;Reks;432");
            testData.add("18;Peks;23423");
            testData.add("22;Teks;2522");
            testData.add("34;Alex;2523");
            testData.add("54;Maks;25235");
            testData.add("67;Feofan;5234");


            file.createNewFile();

            write(filename,testData);
        }

    }


}
