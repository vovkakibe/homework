package lesson7;

import java.util.ArrayList;

public class AccountImpl implements AccountService {

    @Override
    public void withdraw(int accountId, int amount) {
        operation(accountId, "withdraw", amount);
    }

    @Override
    public void balance(int accountId) {
        operation(accountId, "balance", 0);
    }

    @Override
    public void deposit(int accountId, int amount) {
        operation(accountId, "deposit", amount);
    }

    @Override
    public void transfer(int from, int to, int amount) {
        withdraw(from, amount);
        deposit(to, amount);
    }


    private void operation(int accountId, String type, int amount) {
        String filename = "C:\\project\\1.txt";
        ArrayList<String> data = ServiceReader.readData(filename);

        if (data.size() > 0) {
            for (int i = 0; i < data.size(); i++) {
                int summ;
                String[] inform = data.get(i).split(";");
                String money = inform[2];
                if (inform.length == 3) {
                    String code = inform[0];
                    if (Integer.parseInt(code) == accountId) {
                        switch (type) {
                            case "balance":
                                System.out.println(money);
                                break;
                            case "withdraw":
                                summ = Integer.parseInt(money) - amount;

                                fomatArray(data,accountId,summ);



                                ServiceReader.write(filename, data);
                                break;
                            case "deposit":
                                summ = Integer.parseInt(money) + amount;

                                fomatArray(data,accountId,summ);

                                ServiceReader.write(filename, data);
                                break;
                        }

                    }
                }
            }

        }
    }


    private ArrayList<String> fomatArray(ArrayList<String> data,int accountId,int summ){
        for (int i = 0; i < data.size(); i++) {

            String[] dataParse = data.get(i).split(";");

            String idData = dataParse[0];

            if (Integer.parseInt(idData) == accountId) {
                dataParse[2] = Integer.toString(summ);
            }

            String write = dataParse[0] + ';' + dataParse[1] + ";" + dataParse[2];

            data.set(i,write);
        }
        return data;
    }
}
