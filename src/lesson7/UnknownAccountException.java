package lesson7;

import java.io.PrintStream;

public class UnknownAccountException extends Exception {
    public UnknownAccountException(String message) {
        super(message);
    }

    @Override
    public void printStackTrace(PrintStream s) {
        super.printStackTrace(s);
    }
}
