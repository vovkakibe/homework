import java.util.Scanner;

public class Third {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Input a number: ");

        int number = in.nextInt();

        if (number >= 0) {
            ++number;
        }

        System.out.println(number);
    }
}
