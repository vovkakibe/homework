import java.util.Scanner;

public class Fourth {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Input a number: ");

        int number = in.nextInt();

        if (number > 0) {
            ++number;
        } else if (number == 0) {
            number = 10;
        } else {
            number -= 2;
        }

        System.out.println(number);
    }
}
