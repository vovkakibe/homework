import java.util.Arrays;
import java.util.Collections;

public class Eigth {

    public static void main(String[] args) {
        Integer[] numbers = {1, -10, 5, 6, 45, 23, -45, -34, 0, 32, 56, -1, 2, -2};


        System.out.println(getMax(numbers));
        System.out.println(getSumm(numbers, "summ"));
        System.out.println(getSummNegative(numbers));
        System.out.println(getSumm(numbers, "size"));
        System.out.println(getAverage(numbers));
    }

    private static Integer getSumm(Integer[] numbers, String type) {
        Integer summ = 0;
        Integer size = 0;

        for (int i = 0; i < numbers.length; i++) {
            Integer number = numbers[i];
            if (number >= 0) {
                ++size;
                summ += number;
            }

        }

        if (type.equals("summ")) {
            return summ;
        } else {
            return size;
        }


    }


    private static Integer getMax(Integer[] numbers) {

        return Collections.max(Arrays.asList(numbers));
    }

    private static Integer getSummNegative(Integer[] numbers) {
        Integer summ = 0;

        for (int i = 0; i < numbers.length; i++) {
            Integer number = numbers[i];

            if (number < 0) {
                int ost = number % 2;

                if (ost == 0) {
                    summ += number;
                }
            }

        }

        return summ;
    }

    private static Integer getAverage(Integer[] numbers) {
        Integer answer = 0;
        Integer summNegative = 0;
        for (int i = 0; i < numbers.length; i++) {
            Integer number = numbers[i];

            if (number < 0) {
                ++summNegative;
                answer += number;
            }
        }
        answer = answer / summNegative;

        return answer;
    }

}

