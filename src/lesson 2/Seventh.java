import java.util.Scanner;

public class Seventh {
    private static String DOT = ".";
    private static String PHRASE = " Стоимость разговора: ";

    public static void main(String[] args) {
        String answer = "";

        Scanner in = new Scanner(System.in);

        int min = 10;
        System.out.print("Input a number: ");

        Integer number = in.nextInt();

        switch (number) {
            case 905:
                answer = getSumm(4.15, min, "Москва");
                break;
            case 194:
                answer = getSumm(1.98, min, "Ростов");
                break;
            case 491:
                answer = getSumm(2.69, min, "Краснодар");
                break;
            case 800:
                answer = getSumm(5.00, min, "Киров");
                break;

            default:
                System.out.println("Код не найден");

        }

        if (!answer.equals("")) {
            System.out.println(answer);
        }


    }

    private static String getSumm(Double price, int min, String city) {
        Double summ = price * min;

        return city + DOT + PHRASE + summ.toString();
    }


}
