import java.util.*;

public class Fifth {
    public static void main(String[] args) {
        int size = 3;

        Scanner in = new Scanner(System.in);

        List<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            System.out.print("Input a number: ");
            numbers.add(in.nextInt());
        }

        System.out.println(Collections.min(numbers));

    }
}
