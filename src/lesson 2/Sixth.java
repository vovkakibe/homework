import java.util.Scanner;

public class Sixth {
    public static void main(String[] args) {

        String answer = "";

        Scanner in = new Scanner(System.in);

        System.out.print("Input a number: ");

        Integer number = in.nextInt();


        if (number == 0) {
            answer += "Нулевое число ";
        } else {
            if (number < 0) {
                answer += "Отрицательное ";
            } else if (number > 0) {
                answer += "Положительное ";
            }

            answer += evenNumbered(number);
        }


        System.out.println(answer);


    }

    public static String evenNumbered(Integer number) {
        String answer = "";

        int ost = number % 2;

        if (ost == 1 || ost == -1) {
            answer = "нечетное ";
        } else {
            answer = "четное ";
        }

        return answer;
    }


}
