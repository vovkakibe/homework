import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class Ninth {
    public static void main(String[] args) {
        Integer[] array1 = {15, 10, 51, -6, -5, 3, -10, -34, 0, 32, 56, -12, 24, -52};
        Integer[] array2 = {15, 10, 0, -6, -5, 3, 0, -34, 0, 32, 56, 0, 24, -52};


        reverse(array1);
        zeroToend(array2);

        System.out.println(Arrays.toString(array1));
        System.out.println(Arrays.toString(array2));
    }


    private static void reverse(Integer[] array) {
        Collections.reverse(Arrays.asList(array));
    }

    private static void zeroToend(Integer[] array) {
        Arrays.sort(array, Comparator.comparing(number -> number == 0));
    }
}
